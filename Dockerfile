FROM registry.git.beagleboard.org/beagleconnect/ti-cgt:main as build
WORKDIR /ti-sdk-src
COPY . /ti-sdk-src/
ENV CMAKE=cmake
RUN make build-ticlang

FROM registry.git.beagleboard.org/beagleconnect/ti-cgt:main
WORKDIR /ti-sdk
COPY --from=build /ti-sdk-src/build/ticlang/* /ti-sdk/